module.exports = {
    entry: {
        index: './src/index.js'
    },
    mode: 'development',
    module: {
        rules: [{
            exclude: /node_modules/,
            test: /\.js$/,
            use: [
                'babel-loader'
            ]
        }]
    },
    output: {
        filename: '[name]bundle.js',
        path: __dirname + '/dist'
    }
}