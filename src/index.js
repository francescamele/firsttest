import Chart from 'chart.js/auto';

const chart = document.getElementById('myChart').getContext('2d');
const skt = new WebSocket('ws://localhost:9000');

let data = [];
let labels = [];

const myChart = new Chart(chart, {
    type: 'line',
    data: {
        labels,
        datasets: [{
            label: 'Test Chart',
            data,
            backgroundColor: [
                'rgba(0, 132, 64, 0.8)',
            ],
            borderColor: [
                'rgba(6, 33, 195, 0.8)'
            ],
            borderWidth: 2
        }]
    },
    options: {
        scales: {
            // Make the y axis fixed:
            y: {
                min: 0,
                max: 100,
                beginAtZero: true
            }
        },

    }
});

skt.addEventListener("message", ev => {
    // Make the ev.data an object
    let object = JSON.parse(ev.data);
    
    // Take the value of the key 'value' and 
    // turn it into a number
    let dataY = +object.value;
    // Take the value of the key 'time'
    let labelX = object.time;

    // Push the value of 'time' in 'labels'
    myChart.data.labels.push(labelX);

    // Push the value of 'value' in 'data'
    myChart.data.datasets[data.push(dataY)];

    // Alternatively, following chart.js's doc:
    // myChart.data.datasets.forEach(dataset => {
    //     dataset.data.push(dataY);
    // })

    // Update the chart
    myChart.update();
});